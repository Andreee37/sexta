﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ci_cd_demo.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
